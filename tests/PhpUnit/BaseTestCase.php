<?php
/**
 * NOTICE OF LICENSE
 *
 * This source file is licensed exclusively to Company Name
 *
 * @author      Thibaut Gauvin [gauvin.thibaut83@gmail.com]
 */

namespace App\Tests\PhpUnit;

use Doctrine\ORM\EntityManager;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\BrowserKit\Client;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\DomCrawler\Crawler;
use Symfony\Component\Routing\Router;

/**
 * Class BaseTestCase.
 */
abstract class BaseTestCase extends WebTestCase
{
    /**
     * @var Client
     */
    private $client;

    /**
     * @var EntityManager
     */
    private $em;

    /**
     * @var Router
     */
    private $router;

    /**
     * Use this function to connect to user in the application using HTTP less mode
     * See config/package/test/security.yml.
     *
     * @param string $username
     * @param string $password
     *
     * @return Client
     */
    protected function connectUser(string $username, string $password): Client
    {
        $this->client = static::createClient([], [
            'PHP_AUTH_USER' => $username,
            'PHP_AUTH_PW'   => $password,
        ]);

        return $this->client;
    }

    /**
     * @return ContainerInterface|null
     */
    protected function getContainer(): ?ContainerInterface
    {
        return static::$kernel->getContainer();
    }

    /**
     * Get Entity Manager.
     *
     * @return EntityManager
     */
    protected function getManager(): EntityManager
    {
        if (!$this->em) {
            $this->em = $this->getContainer()->get('doctrine.orm.entity_manager');
        }

        return $this->em;
    }

    /**
     * Generate a route.
     *
     * @param string $routeName
     * @param array  $routeParameter
     *
     * @return string
     */
    protected function generateRoute(string $routeName, array $routeParameter = []): string
    {
        if (!$this->router) {
            $this->router = $this->getContainer()->get('router');
        }

        return $this->router->generate($routeName, $routeParameter, false);
    }

    /**
     * Assert that client response is a redirection to given Route Name.
     *
     * @param Client $client
     * @param $routeName
     * @param array  $routeParameter
     * @param string $message
     */
    protected function assertRedirectTo(Client $client, string $routeName, array $routeParameter = [], string $message = 'The response is a not a redirection to given route !'): void
    {
        $redirectUrl = $this->generateRoute($routeName, $routeParameter);
        $this->assertTrue($client->getResponse()->isRedirect($redirectUrl), $message);
    }

    /**
     * Assert that the given content is present in the html page.
     *
     * @param Crawler $crawler
     * @param $content
     * @param string $message
     */
    protected function assertHtmlContains(Crawler $crawler, $content, $message = 'The given html is NOT present in the client response !'): void
    {
        $this->assertGreaterThan(
            0,
            $crawler->filter(sprintf('html:contains("%s")', $content))->count(),
            $message
        );
    }

    /**
     * Assert that the given content is NOT present in the html page.
     *
     * @param Crawler $crawler
     * @param $content
     * @param string $message
     */
    protected function assertHtmlNotContains(Crawler $crawler, $content, string $message = 'The given html IS present in the client response'): void
    {
        $this->assertEquals(
            0,
            $crawler->filter(sprintf('html:contains("%s")', $content))->count(),
            $message
        );
    }

    /**
     * Assert that given content is visible in flash message.
     *
     * @param Crawler $crawler
     * @param $content
     * @param string $message
     */
    protected function assertFlashMessageContains(Crawler $crawler, $content, string $message = 'The flash message is not present !'): void
    {
        $this->assertTrue(
            $crawler->filter('.alert:contains("'.$content.'")')->count() > 0,
            $message
        );
    }

    /**
     * Assert that client response is equal to status code.
     *
     * @param int $expectedStatusCode
     * @param $client
     * @param string $message
     */
    protected function assertStatusCode(int $expectedStatusCode, Client $client, string $message = 'The client response don\'t match expected status code.'): void
    {
        $this->assertEquals($expectedStatusCode, $client->getResponse()->getStatusCode(), $message);
    }
}
