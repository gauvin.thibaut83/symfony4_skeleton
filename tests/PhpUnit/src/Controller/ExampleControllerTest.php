<?php
/**
 * NOTICE OF LICENSE
 *
 * This source file is licensed exclusively to Company Name
 *
 * @author      Thibaut Gauvin [gauvin.thibaut83@gmail.com]
 */

namespace App\Tests\PhpUnit\src\Controller;

use App\Tests\PhpUnit\BaseTestCase;

/**
 * Class ExampleControllerTest.
 *
 * @see ExampleController
 */
class ExampleControllerTest extends BaseTestCase
{
    /**
     * example test.
     */
    public function testIndexAction()
    {
        $anonymousClient = static::createClient();

        $url = $this->generateRoute('homepage_redirect');
        $anonymousClient->request('GET', $url);

        $this->assertRedirectTo($anonymousClient, 'homepage');

        $crawler = $anonymousClient->followRedirect();
        $this->assertHtmlContains($crawler, 'hello bar !');
    }
}
