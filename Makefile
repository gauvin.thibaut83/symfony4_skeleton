-include .env
export

COLOR=\033[0;32m
NC=\033[0m
BOLD=\033[1m

COMPOSE_CMD=docker-compose -f docker-compose.yml
PHP_SERVICE=php

##
## ---------------------
## Available make target
## ---------------------
##

all: help
help:
	@grep -E '(^[a-zA-Z0-9_-.]+:.*?##.*$$)|(^##)' Makefile | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[32m%-30s\033[0m %s\n", $$1, $$2}' | sed -e 's/\[32m##/[33m/'

##
## Provisioning
## ------------
##

init: ## Init .env files with default development values
	cp -i .env.dev .env
	sed -i 's/<REMOTE_HOST>/$(shell hostname -I | grep -Eo "192\.168\.[0-9]{,2}\.[0-9]+")/g' .env
	sed -i 's/<DOCKER_USER>/$(shell echo $$USER)/g' .env
	sed -i 's/<DOCKER_USER_ID>/$(shell id -u $$USER)/g' .env
	@echo "${COLOR}[INFOS]${NC} New '.env' file generated from default development values"

build: ## Build Docker images and ensure they are up to date
	@${COMPOSE_CMD} build

install: build start ## Install application (depend on build & start)
	@${COMPOSE_CMD} run --rm ${PHP_SERVICE} composer install --no-interaction

##
## Docker
## ------
##

start: ## Start containers
	@${COMPOSE_CMD} up -d
	@${COMPOSE_CMD} ps

stop: ## Stop containers
	@${COMPOSE_CMD} down --remove-orphans

restart: stop start ## Restart containers

logs: ## Display running containers logs (Press "Ctrl + c" to exit)
	@${COMPOSE_CMD} logs -f

ssh: ## Start new bash terminal inside the php container
	${COMPOSE_CMD} run --rm ${PHP_SERVICE} bash

##
## Tools
## ------
##

docker.ci.release: ## Build & release a fresh php docker image for CI
	@docker login https://registry.gitlab.com
	docker build -t registry.gitlab.com/gauvin.thibaut83/symfony4_skeleton:ci --build-arg DOCKER_USER=devops --build-arg DOCKER_USER_ID=1000 -f .docker/Dockerfile .docker
	docker push registry.gitlab.com/gauvin.thibaut83/symfony4_skeleton:ci

test: ## Run Phpunit tests
	@${COMPOSE_CMD} run --rm ${PHP_SERVICE} composer test

lint: ## Run Php cs-fixer
	@${COMPOSE_CMD} run --rm ${PHP_SERVICE} composer lint
