<?php
/**
 * NOTICE OF LICENSE
 *
 * This source file is licensed exclusively to Company Name
 *
 * @author      Thibaut Gauvin [gauvin.thibaut83@gmail.com]
 */

namespace App\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class ExampleController.
 *
 * @see ExampleControllerTest
 */
class ExampleController extends Controller
{
    /**
     * @Route(path="/", name="homepage", methods={"GET"})
     *
     * @Template("homepage.html.twig")
     *
     * @return array
     */
    public function indexAction()
    {
        return [
            'name' => 'bar',
        ];
    }

    /**
     * @Route(path="/redirect", name="homepage_redirect", methods={"GET"})
     *
     * @return RedirectResponse
     */
    public function redirectAction()
    {
        return $this->redirectToRoute('homepage');
    }
}
