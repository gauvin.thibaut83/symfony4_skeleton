# Symfony 4 Skeleton:

## Installation:

A Makefile is used to provide some useful shortcuts for manipulating docker & docker-compose commands

        $ make init
        $ make install

Then add entries in `/etc/hosts`

        127.0.0.1   symfony4_skeleton.localhost

**-----------------------------------------------------**

## Makefile "tasks" :

    # List available make commands:
    $ make help
